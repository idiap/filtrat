# -*- coding: utf-8 -*-

from nova2.population import Population
from nova2.problems import Problems
from nova2.treatment import Treatment
from nova2.cohort import Cohort
from nova2.variable import Variable
from nova2.visits import Visits
from nova2.eq import EQ
from nova2.paths import Paths
from utils import misc
import datetime

paths = Paths('2012')

# Study period
begin_study = datetime.date(2006,7,1)
end_study   = datetime.date(2011,12,31)

# Cohort intro period
intro_from = datetime.date(2006,7,1)
intro_until = datetime.date(2011,12,31)

# Initial population
population = Population(filename = paths.get('sidiapq'), cmbd_path=paths.get('cmbd'))

# Population age
min_age = 35
max_age = 75

# Exclusion 
l_antecedents = ['ami_atc', 'angor', 'stroke_i', 'stroke_e', 'stroke_h', 'pad', 'tia', 'ihd_chronic', 'ihd_acute']
l_procedures = ['card_proc']


hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'pad', 'tia']


# Problems at dintro
probs = ['diabetes', 'htn', 'dyslipidemia', 'smoking', 'obesity', 'alcoholism']

# Variables at dintro
vnames = ['tas', 'tad', 'imc', 'coltot', 'colhdl', 'colldl', 'tg', 'cre_all', 'cre_tra',
          'alb', 'alcohol', 'tabac', 'pes',  'talla', 'glu']

# Facturation at dintro (variable indicates if a facturation of given med exists during
# last i months 
meds_persistence = {'aspirine': 3, 'statine': 6, 'c10-no-statine': 6, 'g03a': 3,  'c02': 3, 'c03': 3, 'c07': 3,
                    'c08': 3, 'c09': 3, 'a10': 3, 'n06': 6, 'm01': 3, 'n05': 6, 'h02': 3 }

# Incidence
# Problem and procedures incidence
incidence_probs = hard_ep 
incidence_procs = ['card_proc', 'peri_proc', 'stroke_proc']

# Problems creation using population
problems = Problems(paths)

# Defining the cohort
cohort = Cohort(population = population, 
                beginning = begin_study, 
                ending = end_study, 
                minIntro = intro_from, 
                maxIntro = intro_until)

#### Filters applied to the cohort
cohort.setStatusFilter(population)    # Alive, death or tranferred filter
#### 2014-02-07: S'elimina la gent que ha mort o ha estat traslladat als sis mesos de la mínima data d'entrada
cohort.setMaxIntroDate( misc.addToDate(cohort._maxIntro, datetime.timedelta(-6 * 30)) )

#n=2022460
cohort.setAgeFilter(population, min_age, max_age)  # Age filter

#### Filters applied to the cohort
#### 2014-02-07: S'afageix la condició d'estar lliure de criteris d'exclusió durant 6 mesos

ev = problems.get_events(cohort.cohort, l_antecedents)
ev2 = misc.evPosterior(ev, datetime.date(1900, 1, 1))
cohort.setMaxIntroDate( misc.addToDate( problems.get_absolute_minimum(ev2), datetime.timedelta(-6 * 30) ) )
pr = problems.get_procedures(cohort.cohort, l_procedures)
pr2 = misc.evPosterior(pr, datetime.date(1900, 1, 1))
cohort.setMaxIntroDate( misc.addToDate( problems.get_absolute_minimum(pr2), datetime.timedelta(-6 * 30) ) )

#
#### 2014-02-07: S'elimina la condició de canviar la data d'entrada per trobar un colesterol
#
#colesterol = Variable(population = cohort.cohort, filename = paths.variable('coltot')).firstMeasure(cohort.cohort, before = cohort._maxIntro, onlyDate=True)
#col_post = dict()
#for ocip in cohort.cohort:
#    if ocip in colesterol and cohort._minIntro[ocip] < colesterol[ocip] and colesterol[ocip] < cohort._maxIntro[ocip]:
#        col_post[ocip] = colesterol[ocip]
#cohort.setMinIntroDate(col_post)

#
#### 2014-02-07: L'entrada és defineix a dintro i les mesures a dbasal+-6m
#
basal = cohort._minIntro
maxbasal = misc.addToDate(basal, datetime.timedelta(6*30))
minbasal = misc.addToDate(basal, datetime.timedelta(-6*30))

cohort.setIntro(maxbasal, force=True)

cohort.addData(basal, 'dbasal')
cohort.addData(cohort.cohortIntro, 'dintro')
cohort.addData(population.ageAt(basal), 'age')
cohort.addData(population.sex(cohort.cohort), 'sex')

cohort.addData(misc.getValue(paths.get('medea')), 'medea')

eq = EQ(population, eqafile = paths.get('eqa'), eqffile = paths.get('eqf'), relfile = paths.get('rel'))
cohort.addData(eq.getEQs(basal), ['UBA', 'EQA', 'EQF'])

cohort.addData(cohort._exitus_reason, 'exitus')
cohort.addData(cohort._exitus, 'dexitus')

## 2014-02-07: A partir de dintro és on mesuren les variables
visits = Visits(paths.get('visits'))
cohort.addData( visits.numbervisits(cohort.cohortIntro) , 'visits', default_na = 0)

problems.filtering(cohort.cohort)

evts = problems.get_events(cohort.cohort, probs, before=cohort.cohortIntro)
cohort.addBinaryData(evts, probs)

for vname in vnames:
    var = Variable(population = cohort.cohort, filename = paths.variable(vname))
    if vname != 'cre_all':
        if vname in ['pes', 'talla', 'imc']:
            cohort.addData(var.lastMeasure(cohort.cohort, before=maxbasal), ['d' + vname, vname])
        else:
            cohort.addData(var.lastMeasure(cohort.cohort, after= minbasal, before=maxbasal), ['d' + vname, vname])
    else:
        cohort.addData(var.lastMeasure(cohort.cohort, after= minbasal, before=maxbasal), ['dcre', 'cre'])

for m in meds_persistence:
    treatment = Treatment(population = cohort.cohort, filename = paths.treatment(m), catalog = paths.get('catalog') )
    effect = datetime.timedelta(30 * meds_persistence[m])
    cohort.addData(treatment.statusAt(cohort.cohortIntro, effect = effect), m)

# EVENT INCIDENCE
evts = problems.get_events(cohort.cohort, incidence_probs, after = cohort.cohortIntro, before = cohort._exitus)
cohort.addData(problems.get_relative(evts, incidence_probs, which=0), ['ep_' + i for i in incidence_probs], default_na = 'NA')

procs = problems.get_procedures(cohort.cohort, incidence_procs, after = cohort.cohortIntro, before = cohort._exitus)
cohort.addData(problems.get_relative(procs, incidence_procs, which=0), ['ep_' + i for i in incidence_procs], default_na = 'NA')


cohort.writeTable("filtrat.csv")
